const tailwindcss = require('tailwindcss')
const autoPrefix = require('autoprefixer')

module.exports = {
  plugins: [tailwindcss('./tailwind.js'), autoPrefix],
}
