import { shallow } from 'enzyme'
import React from 'react'

import HomePage from '../../src/pages/home'

describe('map page', () => {
  let wrapper: any
  let useEffect: any

  const mockUseEffect = () => {
    useEffect.mockImplementationOnce(f => f())
  }

  beforeEach(() => {
    useEffect = jest.spyOn(React, 'useEffect').mockImplementation(f => f())

    mockUseEffect()
    mockUseEffect()
    wrapper = shallow(<HomePage />)
  })

  describe('on start', () => {
    it('renders the loading spinner', () => {
      const htmlResult = wrapper.html()

      // unique id
      expect(htmlResult).toMatch(/loading-spinner/)
      // match css class
      expect(htmlResult).toMatch(/spinner/)
      expect(htmlResult).not.toMatch(/somethingdonothave/)
    })
  })
})
