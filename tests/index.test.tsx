import React from 'react'
import ReactDOM from 'react-dom'
import { act, isDOMComponent } from 'react-dom/test-utils'

import MapPage from '../src/pages/map'

let container: HTMLElement

beforeEach(() => {
  container = document.createElement('div')
  document.body.appendChild(container)
})

it('Map is rendered in the container under react DOM', () => {
  // Render Map in the document
  act(() => {
    ReactDOM.render(<MapPage />, container)
  })

  // get the full screen container
  const fullscreenDiv: any = container.querySelector('.fullscreen')

  // expect.. TODO
  expect(isDOMComponent(container)).toBe(true)
  expect(isDOMComponent(fullscreenDiv)).toBe(true)
})
