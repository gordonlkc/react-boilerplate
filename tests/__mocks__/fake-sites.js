const fakeSites = [
  {
    sensors: [
      {
        type: 'Air Temperature',
        value: '22',
        lastReading: '2019-12-02T04:02:26.004Z',
      },
      {
        type: 'Soil Moisture',
        value: '13',
        lastReading: '2019-12-02T04:02:37.275Z',
      },
    ],
    siteDescription:
      '<p>lorem ipsom</p><ul><li>Feature 1</li><li>Feature 2</li></ul><a>Link to website here</a>',
    siteImage: 'http://lorempixel.com/640/480/nature',
    siteLocation: {
      latitude: -35.0933144491,
      longitude: 138.6260661369,
    },
    siteName: 'Licensed Cotton Hat',
    id: '1',
  },
  {
    sensors: [
      {
        type: 'Air Temperature',
        value: '25',
        lastReading: '2019-12-02T04:02:53.978Z',
      },
      {
        type: 'Soil Moisture',
        value: '77',
        lastReading: '2019-12-02T04:01:55.872Z',
      },
    ],
    siteDescription:
      '<p>lorem ipsom</p><ul><li>Feature 1</li><li>Feature 2</li></ul><a>Link to website here</a>',
    siteImage: 'http://lorempixel.com/640/480/nature',
    siteLocation: {
      latitude: -34.9508675523,
      longitude: 138.6706012233,
    },
    siteName: 'Ergonomic Plastic Mouse',
    id: '2',
  },
  {
    sensors: [
      {
        type: 'Air Temperature',
        value: '70',
        lastReading: '2019-12-02T04:01:42.584Z',
      },
      {
        type: 'Soil Moisture',
        value: '66',
        lastReading: '2019-12-02T04:02:49.415Z',
      },
    ],
    siteDescription:
      '<p>lorem ipsom</p><ul><li>Feature 1</li><li>Feature 2</li></ul><a>Link to website here</a>',
    siteImage: 'http://lorempixel.com/640/480/nature',
    siteLocation: {
      latitude: -35.0391524375,
      longitude: 138.6264563261,
    },
    siteName: 'Intelligent Fresh Chicken',
    id: '3',
  },
  {
    sensors: [
      {
        type: 'Air Temperature',
        value: '37',
        lastReading: '2019-12-02T04:02:16.986Z',
      },
      {
        type: 'Soil Moisture',
        value: '7',
        lastReading: '2019-12-02T04:02:24.336Z',
      },
    ],
    siteDescription:
      '<p>lorem ipsom</p><ul><li>Feature 1</li><li>Feature 2</li></ul><a>Link to website here</a>',
    siteImage: 'http://lorempixel.com/640/480/nature',
    siteLocation: {
      latitude: -34.8172464033,
      longitude: 138.6008438278,
    },
    siteName: 'Refined Soft Ball',
    id: '4',
  },
  {
    sensors: [
      {
        type: 'Air Temperature',
        value: '64',
        lastReading: '2019-12-02T04:02:21.327Z',
      },
      {
        type: 'Soil Moisture',
        value: '49',
        lastReading: '2019-12-02T04:01:41.441Z',
      },
    ],
    siteDescription:
      '<p>lorem ipsom</p><ul><li>Feature 1</li><li>Feature 2</li></ul><a>Link to website here</a>',
    siteImage: 'http://lorempixel.com/640/480/nature',
    siteLocation: {
      latitude: -34.8229580245,
      longitude: 138.5277859386,
    },
    siteName: 'Unbranded Steel Salad',
    id: '5',
  },
  {
    sensors: [
      {
        type: 'Air Temperature',
        value: '79',
        lastReading: '2019-12-02T04:01:56.844Z',
      },
      {
        type: 'Soil Moisture',
        value: '67',
        lastReading: '2019-12-02T04:02:04.631Z',
      },
    ],
    siteDescription:
      '<p>lorem ipsom</p><ul><li>Feature 1</li><li>Feature 2</li></ul><a>Link to website here</a>',
    siteImage: 'http://lorempixel.com/640/480/nature',
    siteLocation: {
      latitude: -35.0592794929,
      longitude: 138.6840369685,
    },
    siteName: 'Refined Fresh Keyboard',
    id: '6',
  },
  {
    sensors: [
      {
        type: 'Air Temperature',
        value: '24',
        lastReading: '2019-12-02T04:02:01.127Z',
      },
      {
        type: 'Soil Moisture',
        value: '54',
        lastReading: '2019-12-02T04:02:42.862Z',
      },
    ],
    siteDescription:
      '<p>lorem ipsom</p><ul><li>Feature 1</li><li>Feature 2</li></ul><a>Link to website here</a>',
    siteImage: 'http://lorempixel.com/640/480/nature',
    siteLocation: {
      latitude: -34.9371350821,
      longitude: 138.7902093639,
    },
    siteName: 'Awesome Metal Pants',
    id: '7',
  },
  {
    sensors: [
      {
        type: 'Air Temperature',
        value: '68',
        lastReading: '2019-12-02T04:01:46.651Z',
      },
      {
        type: 'Soil Moisture',
        value: '5',
        lastReading: '2019-12-02T04:02:04.794Z',
      },
    ],
    siteDescription:
      '<p>lorem ipsom</p><ul><li>Feature 1</li><li>Feature 2</li></ul><a>Link to website here</a>',
    siteImage: 'http://lorempixel.com/640/480/nature',
    siteLocation: {
      latitude: -34.9457317929,
      longitude: 138.5685271552,
    },
    siteName: 'Tasty Rubber Ball',
    id: '8',
  },
  {
    sensors: [
      {
        type: 'Air Temperature',
        value: '26',
        lastReading: '2019-12-02T04:01:49.854Z',
      },
    ],
    siteDescription:
      '<p>lorem ipsom</p><ul><li>Feature 1</li><li>Feature 2</li></ul><a>Link to website here</a>',
    siteImage: 'http://lorempixel.com/640/480/nature',
    siteLocation: {
      latitude: -34.8718052516,
      longitude: 138.7454320456,
    },
    siteName: 'Handcrafted Frozen Soap',
    id: '9',
  },
  {
    sensors: [
      {
        type: 'Air Temperature',
        value: '18',
        lastReading: '2019-12-02T04:02:04.381Z',
      },
    ],
    siteDescription:
      '<p>lorem ipsom</p><ul><li>Feature 1</li><li>Feature 2</li></ul><a>Link to website here</a>',
    siteImage: 'http://lorempixel.com/640/480/nature',
    siteLocation: {
      latitude: -34.8797969688,
      longitude: 138.5779214124,
    },
    siteName: 'Sleek Steel Sausages',
    id: '10',
  },
  {
    sensors: [
      {
        type: 'Air Temperature',
        value: '9',
        lastReading: '2019-12-02T04:01:51.805Z',
      },
      {
        type: 'Soil Moisture',
        value: '27',
        lastReading: '2019-12-02T04:02:18.653Z',
      },
    ],
    siteDescription:
      '<p>lorem ipsom</p><ul><li>Feature 1</li><li>Feature 2</li></ul><a>Link to website here</a>',
    siteImage: 'http://lorempixel.com/640/480/nature',
    siteLocation: {
      latitude: -34.9989373295,
      longitude: 138.6708924685,
    },
    siteName: 'Gorgeous Soft Fish',
    id: '11',
  },
  {
    sensors: [
      {
        type: 'Air Temperature',
        value: '60',
        lastReading: '2019-12-02T04:02:01.066Z',
      },
      {
        type: 'Soil Moisture',
        value: '77',
        lastReading: '2019-12-02T04:02:03.807Z',
      },
    ],
    siteDescription:
      '<p>lorem ipsom</p><ul><li>Feature 1</li><li>Feature 2</li></ul><a>Link to website here</a>',
    siteImage: 'http://lorempixel.com/640/480/nature',
    siteLocation: {
      latitude: -34.8846915105,
      longitude: 138.6340004398,
    },
    siteName: 'Handmade Metal Shirt',
    id: '12',
  },
  {
    sensors: [
      {
        type: 'Air Temperature',
        value: '51',
        lastReading: '2019-12-02T04:02:24.541Z',
      },
      {
        type: 'Soil Moisture',
        value: '49',
        lastReading: '2019-12-02T04:02:54.306Z',
      },
    ],
    siteDescription:
      '<p>lorem ipsom</p><ul><li>Feature 1</li><li>Feature 2</li></ul><a>Link to website here</a>',
    siteImage: 'http://lorempixel.com/640/480/nature',
    siteLocation: {
      latitude: -34.9303957541,
      longitude: 138.7892994514,
    },
    siteName: 'Tasty Concrete Chair',
    id: '13',
  },
  {
    sensors: [
      {
        type: 'Air Temperature',
        value: '5',
        lastReading: '2019-12-02T04:02:14.768Z',
      },
      {
        type: 'Soil Moisture',
        value: '13',
        lastReading: '2019-12-02T04:01:32.753Z',
      },
    ],
    siteDescription:
      '<p>lorem ipsom</p><ul><li>Feature 1</li><li>Feature 2</li></ul><a>Link to website here</a>',
    siteImage: 'http://lorempixel.com/640/480/nature',
    siteLocation: {
      latitude: -34.9167288981,
      longitude: 138.6073713892,
    },
    siteName: 'Licensed Rubber Gloves',
    id: '14',
  },
  {
    sensors: [
      {
        type: 'Air Temperature',
        value: '26',
        lastReading: '2019-12-02T04:01:43.449Z',
      },
      {
        type: 'Soil Moisture',
        value: '21',
        lastReading: '2019-12-02T04:02:47.428Z',
      },
    ],
    siteDescription:
      '<p>lorem ipsom</p><ul><li>Feature 1</li><li>Feature 2</li></ul><a>Link to website here</a>',
    siteImage: 'http://lorempixel.com/640/480/nature',
    siteLocation: {
      latitude: -34.9587033268,
      longitude: 138.6419698679,
    },
    siteName: 'Generic Steel Table',
    id: '15',
  },
  {
    sensors: [
      {
        type: 'Air Temperature',
        value: '66',
        lastReading: '2019-12-02T04:02:19.715Z',
      },
      {
        type: 'Soil Moisture',
        value: '32',
        lastReading: '2019-12-02T04:02:00.894Z',
      },
    ],
    siteDescription:
      '<p>lorem ipsom</p><ul><li>Feature 1</li><li>Feature 2</li></ul><a>Link to website here</a>',
    siteImage: 'http://lorempixel.com/640/480/nature',
    siteLocation: {
      latitude: -35.0379644663,
      longitude: 138.5149582025,
    },
    siteName: 'Licensed Rubber Keyboard',
    id: '16',
  },
  {
    sensors: [
      {
        type: 'Air Temperature',
        value: '12',
        lastReading: '2019-12-02T04:02:09.558Z',
      },
      {
        type: 'Soil Moisture',
        value: '35',
        lastReading: '2019-12-02T04:02:24.581Z',
      },
    ],
    siteDescription:
      '<p>lorem ipsom</p><ul><li>Feature 1</li><li>Feature 2</li></ul><a>Link to website here</a>',
    siteImage: 'http://lorempixel.com/640/480/nature',
    siteLocation: {
      latitude: -34.8539869821,
      longitude: 138.6616463477,
    },
    siteName: 'Rustic Plastic Computer',
    id: '17',
  },
  {
    sensors: [
      {
        type: 'Air Temperature',
        value: '71',
        lastReading: '2019-12-02T04:01:26.220Z',
      },
      {
        type: 'Soil Moisture',
        value: '9',
        lastReading: '2019-12-02T04:01:50.318Z',
      },
    ],
    siteDescription:
      '<p>lorem ipsom</p><ul><li>Feature 1</li><li>Feature 2</li></ul><a>Link to website here</a>',
    siteImage: 'http://lorempixel.com/640/480/nature',
    siteLocation: {
      latitude: -34.9873131439,
      longitude: 138.5498810079,
    },
    siteName: 'Generic Soft Bike',
    id: '18',
  },
]

export default fakeSites
