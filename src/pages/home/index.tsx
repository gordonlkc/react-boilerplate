/* eslint-disable global-require */
import React, { useEffect, useRef, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { actions, getters } from '../../stores/index'

const HomePage = () => {
  // -------- effects
  const [, setIsLoading] = useState(true)
  const isMounted = useRef(true)
  // counter A getters
  const counterACount = useSelector(getters.counterA.countSelector)
  const counterACountTimesTen = useSelector(
    getters.counterA.countTimesTenSelector,
  )
  // counter B getters
  const counterBCount = useSelector(getters.counterB.countSelector)
  const counterBCountTimesTen = useSelector(
    getters.counterB.countTimesTenSelector,
  )
  // counter C getters
  const counterCCount = useSelector(getters.counterC.countSelector)
  const counterCCountTimesTen = useSelector(
    getters.counterC.countTimesTenSelector,
  )
  const dispatch = useDispatch()

  // -------- Data
  useEffect(() => {
    isMounted.current = false
    setIsLoading(false)
  }, [])

  const A = () => {
    return (
      <div className="m-8">
        <h2>CountA: {counterACount}</h2>
        <h2>CountA * 10: {counterACountTimesTen}</h2>
        <div className="flex items-center justify-center">
          <button onClick={() => dispatch(actions.counterA.increment(1))}>
            +
          </button>
          <button onClick={() => dispatch(actions.counterA.decrement(1))}>
            -
          </button>
        </div>
      </div>
    )
  }

  const B = () => {
    return (
      <div className="m-8">
        <h2>CountB: {counterBCount}</h2>
        <h2>CountB * 10: {counterBCountTimesTen}</h2>
        <div className="flex items-center justify-center">
          <button onClick={() => dispatch(actions.counterB.increment(1))}>
            +
          </button>
          <button onClick={() => dispatch(actions.counterB.decrement(1))}>
            -
          </button>
        </div>
      </div>
    )
  }

  const C = () => {
    return (
      <div className="m-8">
        <h2>CountC: {counterCCount}</h2>
        <h2>CountC * 10: {counterCCountTimesTen}</h2>
        <div className="flex items-center justify-center">
          <button onClick={() => dispatch(actions.counterC.increment(1))}>
            +
          </button>
          <button onClick={() => dispatch(actions.counterC.decrement(1))}>
            -
          </button>
        </div>
      </div>
    )
  }

  return (
    <div className="absolute inset-0 flex flex-col items-center justify-center">
      <div className="max-w-sm rounded overflow-hidden shadow-lg p-10">
        <h1> Persistent local stoage</h1>
        <A />
        <h1> Persistent cookies stoage</h1>
        <B />
        <h1> Non persistent </h1>
        <C />
      </div>
    </div>
  )
}

export default HomePage
