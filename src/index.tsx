import 'normalize.css'
import * as React from 'react'
import 'react-app-polyfill/ie11'
import * as ReactDOM from 'react-dom'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import { Provider } from 'react-redux'
import { store, persistor } from './stores'
import { PersistGate } from 'redux-persist/integration/react'


import HomePage from './pages/home'

import * as serviceWorker from './serviceWorker'

import './styles/app.css'
import './styles/tailwind.css'

import { startMirage } from './utils/mirage'

// components

// start mirage in development env only
if (
  process.env.REACT_APP_STAGE === 'dev' ||
  process.env.REACT_APP_STAGE === 'test'
) {
  console.log('staring with mirage mock server..')
  startMirage({ environment: process.env.NODE_ENV })
}

const routes = (
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <Router>
        <Route path="/" component={HomePage} />
      </Router>
    </PersistGate>
  </Provider>
)

ReactDOM.render(routes, document.getElementById('app'))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister()
