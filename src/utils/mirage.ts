/* eslint-disable */
import { Factory, Model, Server } from '@miragejs/server'
import faker from 'faker'

const startMirage = ({ environment = 'test' } = {}) => {
  return new Server({
    environment,
    models: {
      person: Model,
    },
    factories: {
      person: Factory.extend({
        firstName() {
          return faker.name.firstName()
        },
        lastName() {
          return faker.name.lastName
        }
      }),
    },
    scenarios: {
      default(server: any) {
        server.createList('person', 20)
      },
    },
    baseConfig() {
      this.namespace = 'data'

      // the following will respond with a JSON payload
      this.get('/dummy', schema => {
        return schema.person.all().models
      })

      // prod api
      this.passthrough('your production api endpoint')
      // staging api
      this.passthrough('your staging api endpoint')
      this.passthrough()
    },
  })
}

export { startMirage }
