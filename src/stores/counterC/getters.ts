import { createSelector } from 'reselect'
import { storeName } from './config'

// pure selector
const countSelector = state => state[storeName].count

// memoized selectors. createSelector takes an array of input-selectors and a transform function as its arguments
const countTimesTenSelector = createSelector(
  countSelector,
  result => result * 10,
)

export { countSelector, countTimesTenSelector }