import { createActions } from 'redux-actions'
import { storeName } from './config'

export const { increment, decrement } = createActions(
  {
    INCREMENT: (amount = 1) => ({ amount }),
    DECREMENT: (amount = 1) => ({ amount: -amount }),
  },
  {
    prefix: storeName,
  },
)
