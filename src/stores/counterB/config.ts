import { CookieStorage } from 'redux-persist-cookie-storage'
import Cookies from 'cookies-js'

// name of store
// prefix of actions
export const storeName = 'counterB'

export const persistConfig = {
  key: storeName,
  storage: new CookieStorage(Cookies/*, options */)
}
