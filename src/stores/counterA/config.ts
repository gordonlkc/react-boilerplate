import storage from 'redux-persist/lib/storage'

// name of store
// prefix of actions
export const storeName = 'counterA'

export const persistConfig = {
  key: storeName,
  storage,
}
