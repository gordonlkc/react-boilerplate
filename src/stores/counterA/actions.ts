import { createActions } from 'redux-actions'
import { storeName } from './config'

export const { increment, decrement } = createActions(
  {
    INCREMENT: amount => ({ amount }),
    DECREMENT: amount => ({ amount: -amount }),
  },
  {
    prefix: storeName,
  },
)
