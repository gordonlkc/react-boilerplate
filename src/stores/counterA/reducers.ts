import { handleActions } from 'redux-actions'
import { increment, decrement } from './actions'

const defaultState = { count: 1 }

const reducer = handleActions(
  {
    [increment]: (state, { payload: { amount } }) => {
      return { ...state, count: state.count + amount }
    },
    [decrement]: (state, { payload: { amount } }) => {
      return { ...state, count: state.count + amount }
    },
  },
  defaultState,
)

export default reducer
