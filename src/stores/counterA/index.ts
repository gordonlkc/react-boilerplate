import * as actions from './actions'
import reducers from './reducers'
import * as getters from './getters'
import { persistConfig } from './config'

export { actions, getters, reducers, persistConfig }
