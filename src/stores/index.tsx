import { combineReducers, createStore } from 'redux'
import { persistStore, persistReducer } from 'redux-persist'
// instance
import * as counterA from './counterA'
import * as counterB from './counterB'
import * as counterC from './counterC'

const rootReducer = combineReducers({
  counterA: persistReducer(counterA.persistConfig, counterA.reducers),
  counterB: persistReducer(counterB.persistConfig, counterB.reducers),
  counterC: counterC.reducers,
})

const actions = {
  counterA: counterA.actions,
  counterB: counterB.actions,
  counterC: counterC.actions,
}

const getters = {
  counterA: counterA.getters,
  counterB: counterB.getters,
  counterC: counterC.getters,
}

const store = createStore(rootReducer)
const persistor = persistStore(store)

export { store, getters, persistor, actions }
