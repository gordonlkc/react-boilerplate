> Demonstrating when using React and TypeScript.

This project was bootstrapped with [Create React App](https://create-react-app.dev/).

## Getting started

A Node.js 10.0.0+ setup with npm is recommended

```bash
# install dependencies
npm

# ...or if you'd like to use npm instead
npm install

# serve with hot reload at localhost:3000
npm run start

# build for production
npm build
```

## Tech Stack

Here's a curated list of packages that you should be at least familiar with before starting your project. However, the best way to see a complete list of the dependencies is to check [package.json]
- React + Typescript
- Redux + React hooks
- Reselect
- redux-persist
- Tailwind Css
- Jest
- TsLint

## Project Structure

Let's start with understanding why I have chosen our particular structure.

In any case, here's the TL;DR:

- You will write your app in the `src` folder. This is the folder you will spend most, if not all, of your time in.


## How Redux is architected?
![redux](./docs/redux.png)
IMO
1.hooks are better to provide ability of reusing logical unit with local state across components.

2.common logic reuse and global state management inspired by VueX

3.global state mapping and performance optimize is what connect & reselect should do.

But we use useSelector in a Container Component where you establish data flow or create handlers for Presentation Component. Thus use useDisptch to mutate an event

## Why to create the boilerplate?

Quite an understandable question!

The primary purpose of this boilerplate is to provide a robust and scalable infrastructure for upcoming Fleet front-end projects. Every bit of your project should be predictable. You should have absolute control over all the state change. 

## In the boilerplate

There are three main things to be demostrated

1. Redux persistant store
2. State management with mutations, getters and actions
3. A touch of tailwind css
![demo](./docs/demo.gif)